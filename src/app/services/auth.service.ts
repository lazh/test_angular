import {Injectable} from "@angular/core";
import {HttpClient,HttpHeaders} from "@angular/common/http";
import {UserService} from "../shared/services/user.service";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable()
export class AuthService {
  private url = '';

  constructor(public httpClient : HttpClient) {
    this.url = environment.url;
  }

  login(credentials : any) : Observable<any> {
    const authCheck = new Observable((observer) => {
      observer.next({
        auth: credentials.user === environment.user && credentials.password === environment.password
      });
    });
    return authCheck;
  }

}
