import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {HttpClient,HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MealService {
  private url = environment.url;
  constructor(public httpClient : HttpClient) { }

  getPlatillosAleatorios() : Observable<any> {
    const header = new HttpHeaders().set('Content-Type','application/json');
    return this.httpClient.get(this.url + 'random.php',{headers: header});
  }

  getListarPlatillos() : Observable<any> {
    const header = new HttpHeaders().set('Content-Type','application/json');
    return this.httpClient.get(this.url + 'search.php?s=Arrabiata',{headers: header});
  }
  getPlatilloById(id: number) : Observable<any> {
    const header = new HttpHeaders().set('Content-Type','application/json');
    return this.httpClient.get(this.url + 'lookup.php?i=' + id,{headers: header});
  }

  getPlatillosByCategory(category: string) : Observable<any> {
    const header = new HttpHeaders().set('Content-Type','application/json');
    return this.httpClient.get(this.url + 'filter.php?c=' + category,{headers: header});
  }
  getPlatillosByName(name: string) : Observable<any> {
    const header = new HttpHeaders().set('Content-Type','application/json');
    return this.httpClient.get(this.url + 'search.php?s=' + name,{headers: header});
  }
  getPlatillosByPrimary(name: string) : Observable<any> {
    const header = new HttpHeaders().set('Content-Type','application/json');
    return this.httpClient.get(this.url + 'filter.php?i=' + name,{headers: header});
  }


}
