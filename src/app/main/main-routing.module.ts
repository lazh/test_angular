import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MenuComponent} from "../auth/components/menu/menu.component";
import {DetalleComponent} from "./components/detalle/detalle.component";

const routes: Routes = [
  {
    path: '', component: MenuComponent,
    children: [
      {
        path: 'detail/:id',
        component: DetalleComponent
      },
      /*{
        path: 'detail',
        component: StudentDetailComponent
      }*/
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
