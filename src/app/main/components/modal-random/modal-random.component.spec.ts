import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRandomComponent } from './modal-random.component';

describe('ModalRandomComponent', () => {
  let component: ModalRandomComponent;
  let fixture: ComponentFixture<ModalRandomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalRandomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRandomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
