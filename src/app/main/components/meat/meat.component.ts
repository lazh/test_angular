import {Component,Input,OnInit,Output, EventEmitter} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-meat',
  templateUrl: './meat.component.html',
  styleUrls: ['./meat.component.css']
})
export class MeatComponent implements OnInit {
  @Input() item: any = {};
  @Output() onSelect = new EventEmitter<string>();
  public tags = [];
  constructor(private router: Router,) { }

  ngOnInit(): void {
    if (this.item.strTags) {
      this.tags = this.item.strTags.split(',');
    }
  }

  openView() {
    if (this.item) {
      console.log('data of', this.item)
      this.onSelect.emit('');
      this.router.navigate(['/detail/' + this.item.idMeal]).then();
    }
  }

}
