import {Component,Input,OnInit} from '@angular/core';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {
  public urlVideo = 'https://www.youtube.com/watch?v=4aZr5hZXP_s';
  @Input() video: string = '';
  constructor() { }

  ngOnInit(): void {
  }

}
