import {Component,OnDestroy,OnInit} from '@angular/core';
import {UserService} from "../../../shared/services/user.service";
import {ActivatedRoute,Router} from "@angular/router";
import {MealService} from "../../../services/meal.service";

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit, OnDestroy {
  public randomItem: any;
  private sub: any;
  public listIngredents = [];
  private id = 0;

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private mealService: MealService) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
      this.getData();
    });
  }

  prepareList(item: any) {

  }

  getData() {
    this.mealService.getPlatilloById(this.id).subscribe(res => {
      if (res.meals) {
        this.randomItem = res.meals[0];
        this.prepareList(this.randomItem)
      }
    }, error => {
      const res = {"meals":[{"idMeal":"52823","strMeal":"Salmon Prawn Risotto","strDrinkAlternate":null,"strCategory":"Seafood","strArea":"Italian","strInstructions":"Melt the butter in a thick-based pan and gently cook the onion without colour until it is soft.\r\nAdd the rice and stir to coat all the grains in the butter\r\nAdd the wine and cook gently stirring until it is absorbed\r\nGradually add the hot stock, stirring until each addition is absorbed. Keep stirring until the rice is tender\r\nSeason with the lemon juice and zest, and pepper to taste. (there will probably be sufficient saltiness from the salmon to not need to add salt) Stir gently to heat through\r\nServe scattered with the Parmesan and seasonal vegetables.\r\nGrill the salmon and gently place onto the risotto with the prawns and asparagus","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/xxrxux1503070723.jpg","strTags":"Fish","strYoutube":"https:\/\/www.youtube.com\/watch?v=V2PMvBv52IE","strIngredient1":"butter","strIngredient2":"onion","strIngredient3":"rice","strIngredient4":"white wine","strIngredient5":"vegetable stock","strIngredient6":"lemon","strIngredient7":"King Prawns","strIngredient8":"salmon","strIngredient9":"asparagus","strIngredient10":"black pepper","strIngredient11":"Parmesan","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"50g\/2oz","strMeasure2":"1 finely chopped ","strMeasure3":"150g","strMeasure4":"125ml ","strMeasure5":"1 litre hot","strMeasure6":"The juice and zest of one","strMeasure7":"240g large","strMeasure8":"150g","strMeasure9":"100g tips blanched briefly in boiling water","strMeasure10":"ground","strMeasure11":"50g shavings","strMeasure12":"","strMeasure13":"","strMeasure14":"","strMeasure15":"","strMeasure16":"","strMeasure17":"","strMeasure18":"","strMeasure19":"","strMeasure20":"","strSource":"http:\/\/www.rangemaster.co.uk\/rangemaster-owners\/recipes-ideas-inspiration\/recipes\/mains\/prawn-and-hot-smoked-salmon-risotto-with-asparagus","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]};
      this.randomItem = res.meals[0];
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
