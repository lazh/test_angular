import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MenuComponent } from '../auth/components/menu/menu.component';
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import { VideoPlayerComponent } from './components/video-player/video-player.component';
import {VgCoreModule} from "@videogular/ngx-videogular/core";
import {VgControlsModule} from "@videogular/ngx-videogular/controls";
import {VgOverlayPlayModule} from "@videogular/ngx-videogular/overlay-play";
import {VgBufferingModule} from "@videogular/ngx-videogular/buffering";
import {MatChipsModule} from "@angular/material/chips";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatTabsModule} from "@angular/material/tabs";
import { MeatComponent } from './components/meat/meat.component';
import { DetalleComponent } from './components/detalle/detalle.component';
import { ModalRandomComponent } from './components/modal-random/modal-random.component';
import {MatDialogModule} from "@angular/material/dialog";


@NgModule({
  declarations: [
    MenuComponent,
    VideoPlayerComponent,
    MeatComponent,
    DetalleComponent,
    ModalRandomComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    MainRoutingModule,
    MatDialogModule,
    SharedModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    MatChipsModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
  ],
  entryComponents: [
    ModalRandomComponent
  ]
})
export class MainModule { }
