import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private identity: any;

  constructor() {
    this.identity = this.getIdentity();
  }

  getIdentity(): any {
    const idUser: string  = localStorage.getItem('identity') ?? '';
    if (!idUser || idUser.length <= 0) {
      return null;
    }
    const identity = JSON.parse(idUser);
    if (identity !== undefined) {
      this.identity = identity;
    } else {
      this.identity = null;
    }
    return this.identity;
  }

  setIdentity(credential: any) {
    localStorage.setItem('identity', JSON.stringify(credential))
  }


}
