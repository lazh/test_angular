import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { LoadingDirective } from './directives/loading.directive';
import { DoubleClickPreventDirective } from './directives/double-click-prevent.directive';
import {HttpClientModule} from "@angular/common/http";
import {MealService} from "../services/meal.service";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    LoadingDirective,
    DoubleClickPreventDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedRoutingModule,
    HttpClientModule
  ],
  providers: [
    MealService,
  ]
})
export class SharedModule { }
