import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../shared/services/user.service";
import {Router} from "@angular/router";
import {MealService} from "../../../services/meal.service";
import {MatDialog} from "@angular/material/dialog";
import {ModalRandomComponent} from "../../../main/components/modal-random/modal-random.component";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public randomItem: any;
  private isOpen = false;

  constructor(private userService: UserService,
              private router: Router,
              private matDialog: MatDialog,
              private mealService: MealService) { }

  ngOnInit(): void {
    if (!this.userService.getIdentity()) {
      this.router.navigate(['/login']).then();
    }
    if (!this.isOpen) {
      this.getRandomMeat();
    }

  }

  getRandomMeat() {
    this.matDialog.open(ModalRandomComponent);
    this.isOpen = true;
    this.mealService.getPlatillosAleatorios().subscribe(res => {
      console.log('data of', res);
      if (res.meals) {
        this.randomItem = res.meals[0];
      }
    }, error => {
      const res = {"meals":[{"idMeal":"52923","strMeal":"Canadian Butter Tarts","strDrinkAlternate":null,"strCategory":"Dessert","strArea":"Canadian","strInstructions":"Preheat the oven to fan 170C\/ conventional 190C\/gas 5. Roll out the pastry on a lightly floured surface so it\u2019s slightly thinner than straight from the pack. Then cut out 18-20 rounds with a 7.5cm fluted cutter, re-rolling the trimmings. Use the rounds to line two deep 12-hole tart tins (not muffin tins). If you only have a regular-sized, 12-hole tart tin you will be able to make a few more slightly shallower tarts.\r\nBeat the eggs in a large bowl and combine with the rest of the ingredients except the walnuts. Tip this mixture into a pan and stir continuously for 3-4 minutes until the butter melts, and the mixture bubbles and starts to thicken. It should be thick enough to coat the back of a wooden spoon. Don\u2019t overcook, and be sure to stir all the time as the mixture can easily burn. Remove from the heat and stir in the nuts.\r\nSpoon the filling into the unbaked tart shells so it\u2019s level with the pastry. Bake for 15-18 minutes until set and pale golden. Leave in the tin to cool for a few minutes before lifting out on to a wire rack. Serve warm or cold.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/wpputp1511812960.jpg","strTags":"Speciality,Snack,Desert,Pudding","strYoutube":"https:\/\/www.youtube.com\/watch?v=WUpaOGghOdo","strIngredient1":"Shortcrust Pastry","strIngredient2":"Eggs","strIngredient3":"Muscovado Sugar","strIngredient4":"Raisins","strIngredient5":"Vanilla Extract","strIngredient6":"Butter","strIngredient7":"Single Cream","strIngredient8":"Walnuts","strIngredient9":"","strIngredient10":"","strIngredient11":"","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"375g","strMeasure2":"2 large","strMeasure3":"175g","strMeasure4":"100g ","strMeasure5":"1 tsp ","strMeasure6":"50g","strMeasure7":"4 tsp","strMeasure8":"50g","strMeasure9":"","strMeasure10":"","strMeasure11":"","strMeasure12":"","strMeasure13":"","strMeasure14":"","strMeasure15":"","strMeasure16":"","strMeasure17":"","strMeasure18":"","strMeasure19":"","strMeasure20":"","strSource":"https:\/\/www.bbcgoodfood.com\/recipes\/1837\/canadian-butter-tarts","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]};
      this.randomItem = res.meals[0];
    });
  }

  onLogout() {
    this.userService.setIdentity(null);
    this.router.navigate(['/login']).then();
  }

}
