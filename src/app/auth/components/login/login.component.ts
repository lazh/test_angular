import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../shared/services/user.service";
import {AuthService} from "../../../services/auth.service";
import {environment} from "../../../../environments/environment";
import {Router} from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public username: any;
  public password: any;

  constructor(private userService : UserService,
              private authService: AuthService,
              private router: Router
  ) { }

  ngOnInit(): void {
  }

  onLogin() {
    const credentials = {
      user: this.username,
      password: this.password,
      profile: {}
    };
    this.authService.login(credentials).subscribe(res => {
      if (res.auth) {
        credentials.profile = environment.profile;
        this.userService.setIdentity(credentials);
        this.router.navigate(['/']).then();
      } else {
        // show alert
        this.router.navigate(['/login']).then();
      }

    }, error => {

    });
  }

}
