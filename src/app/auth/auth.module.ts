import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './components/login/login.component';
import {AuthService} from "../services/auth.service";
import {SharedModule} from "../shared/shared.module";
import {MatIconModule} from "@angular/material/icon";

import {FormsModule} from "@angular/forms";
import {MatChipsModule} from "@angular/material/chips";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {DoubleClickPreventDirective} from "../shared/directives/double-click-prevent.directive";


@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    MatIconModule,
    MatChipsModule,
    MatCheckboxModule,
  ],
  providers: [
    AuthService,
  ]
})
export class AuthModule { }
